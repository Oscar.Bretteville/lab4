package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;
	CellState nextCellState;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row,column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
        
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
		for (int row = 0; row < numberOfRows(); row++) {
			for (int col = 0; col < numberOfColumns(); col++) {
				nextGeneration.set(row,col,getNextCell(row,col));
				
			}
			
		}
		currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        CellState currentCell = currentGeneration.get(row, col);
        int aliveNeighbors = countNeighbors(row, col, CellState.ALIVE);
        if(currentCell == CellState.ALIVE) {
            return CellState.DYING;
        }
        else if (currentCell == CellState.DYING) {
            return CellState.DEAD;
        }
        else if (currentCell == CellState.DEAD && aliveNeighbors == 2) {
            return CellState.ALIVE;
        }
        else {
            return CellState.DEAD;
        }
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
    private int countNeighbors(int row, int col, CellState state) {
		if (row < 0 || row >= numberOfRows() || col<0 || col > numberOfColumns()) {
			throw new IndexOutOfBoundsException();
		}
		int counter = 0;
		for (int i = row-1; i < row+2; i++) {
			for (int j = col-1; j < col+2; j++) {
				if (i<0 || j<0) {
					continue;
				}
				if (i >= currentGeneration.numRows() || j >= currentGeneration.numColumns()) {
					continue;
				}
				if (i == row && j == col) {
					continue;
				}
				else if (currentGeneration.get(i, j).equals(state)) {
					counter++;
				}
				
			}
			
		}
		return counter;
	}

    
    
}
