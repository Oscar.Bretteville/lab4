package datastructure;

import java.util.ArrayList;

import cellular.CellState;

public class CellGrid implements IGrid {

    int cols;
    int rows;
    CellState[][] cellGrid;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;
        cellGrid = new CellState[rows][columns];
        
	}

    private int indexOf(int row, int column) {
        return column+row*column;
    }

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if ((row > numRows()) || (row < 0)) {
            throw new IndexOutOfBoundsException();
        }
        
        if ((column > numColumns()) || (column < 0)) {
            throw new IndexOutOfBoundsException();
        }
        cellGrid[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        if ((row > numRows()) || (row < 0)) {
            throw new IndexOutOfBoundsException();
        }
        if ((column > numColumns()) || (column < 0)) {
            throw new IndexOutOfBoundsException();
        }
        return cellGrid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid localGrid = new CellGrid(this.rows, this.cols, CellState.DEAD);
        for (int row=0; row<rows; row++) {
            for (int col=0; col<cols; col++) {
                localGrid.set(row, col , this.get(row, col));
            }
        }


        return localGrid;
    }
    
}
